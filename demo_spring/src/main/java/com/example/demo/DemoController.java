package com.example.demo;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class DemoController {

    private final DemoService demoService;

    @GetMapping("/test")
    public String test(){
        return "test";
    }

    @GetMapping("/list")
    public List<Note> list(){
        return demoService.list();
    }
}
