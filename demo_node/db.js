const Pool = require('pg').Pool
const pool = new Pool({
  user: 'demo',
  host: 'localhost',
  database: 'demo',
  password: 'demo',
  port: 5432,
})

const getNotes = (request, response) => {
    pool.query('SELECT * FROM Notes ORDER BY note_key ASC', (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
}

module.exports={
    getNotes
}