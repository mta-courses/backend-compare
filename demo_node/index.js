const express = require('express')
const bodyParser = require('body-parser')
const app = express();
const port = 8000;
const db = require('./db');

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.get('/', (req, res) => {
  res.send('Hello World!')
});

app.get('/notes', db.getNotes)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`)
});
